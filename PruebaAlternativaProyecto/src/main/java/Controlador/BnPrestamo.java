/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;


import com.utilidades.Persistencia;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import modelo.dao.EmpleadoDao;
import modelo.dao.LibroDao;
import modelo.dao.PrestamoDao;
import modelo.dao.UsuarioDao;
import modelo.entidad.Empleado;
import modelo.entidad.Libro;
import modelo.entidad.Prestamo;
import modelo.entidad.Usuario;
/**
 *
 * @author elcon
 */
@ManagedBean(name ="PrestamoBean")
@RequestScoped
public class BnPrestamo {
   private Empleado empleado;
   private EmpleadoDao empleadoDao;
   private LibroDao libroDao;
   private Libro libro;
   private Usuario usuario;
   private UsuarioDao usuarioDao;
   private PrestamoDao prestamoDao;
   private Prestamo prestamo;
   
   private List<Prestamo>listaPrestamos;
   private List<Usuario>listaUsuarios;
   private List<Empleado>listaEmpleados;
   private List<Libro>listaLibros;
   private List<Libro>lsitaLibros;
   
   private Date date;
   private Persistencia persistencia;

   @PostConstruct
  public void init(){
   listaPrestamos= new ArrayList();
    listaUsuarios =new ArrayList();
    listaEmpleados =new ArrayList();
     listaLibros=new ArrayList();
    lsitaLibros=new ArrayList();
   
    empleado = new Empleado();
     empleadoDao = new EmpleadoDao();
    libroDao = new LibroDao();
    libro= new Libro();
     usuario =new Usuario();
     usuarioDao= new UsuarioDao();
    prestamoDao= new PrestamoDao();
    prestamo= new Prestamo();
    persistencia= new Persistencia();
    
    llenarListas();
  }

    public Persistencia getPersistencia() {
        return persistencia;
    }

    public void setPersistencia(Persistencia persistencia) {
        this.persistencia = persistencia;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Libro> getListaLibros() {
        return listaLibros;
    }

    public void setListaLibros(List<Libro> listaLibros) {
        this.listaLibros = listaLibros;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }
   
   
    public Prestamo getPrestamo() {
        return prestamo;
    }

    public void setPrestamo(Prestamo prestamo) {
        this.prestamo = prestamo;
    }

    public EmpleadoDao getEmpleadoDao() {
        return empleadoDao;
    }

    public void setEmpleadoDao(EmpleadoDao empleadoDao) {
        this.empleadoDao = empleadoDao;
    }

    public LibroDao getLibroDao() {
        return libroDao;
    }

    public void setLibroDao(LibroDao libroDao) {
        this.libroDao = libroDao;
    }

    public UsuarioDao getUsuarioDao() {
        return usuarioDao;
    }

    public void setUsuarioDao(UsuarioDao usuarioDao) {
        this.usuarioDao = usuarioDao;
    }

    public PrestamoDao getPrestamoDao() {
        return prestamoDao;
    }

    public void setPrestamoDao(PrestamoDao prestamoDao) {
        this.prestamoDao = prestamoDao;
    }

    public List<Prestamo> getListaPrestamos() {
        return listaPrestamos;
    }

    public void setListaPrestamos(List<Prestamo> listaPrestamos) {
        this.listaPrestamos = listaPrestamos;
    }

    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public List<Empleado> getListaEmpleados() {
        return listaEmpleados;
    }

    public void setListaEmpleados(List<Empleado> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }

    public List<Libro> getLsitaLibros() {
        return lsitaLibros;
    }

    public void setLsitaLibros(List<Libro> lsitaLibros) {
        this.lsitaLibros = lsitaLibros;
    }
    
    
    public void mensaje(){
        System.out.println("hola");
    }
    
public void men(){
    System.out.println("hola");
}
public String NombreYApellido(Usuario usuario){
 return  usuario.getNombre()+"-"+usuario.getApellido();
}
public String NombreYApellidoEm(Empleado empleado){
 return  empleado.getNombre()+"-"+empleado.getApellido();
}
public void llenarListas(){
   
       try {
       
           listaEmpleados = persistencia.ConsumirListaEmpleados();
           listaPrestamos = prestamoDao.obtenerPrestamos();
           listaUsuarios= persistencia.ConsumirListaUsuario();
       } catch (ParseException ex) {
           Logger.getLogger(BnPrestamo.class.getName()).log(Level.SEVERE, null, ex);
       } catch (IOException ex) {
           Logger.getLogger(BnPrestamo.class.getName()).log(Level.SEVERE, null, ex);
       } catch (org.json.simple.parser.ParseException ex) {
           Logger.getLogger(BnPrestamo.class.getName()).log(Level.SEVERE, null, ex);
       }
    listaLibros=  libroDao.listarLibros();
    listaUsuarios = usuarioDao.listarUsuarios();
    
   
}
   public void prestamoAgregar() throws ParseException{
     FacesMessage mensaje =new FacesMessage(); 
       
       SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
   
       String date =dateFormat.format(prestamo.getFechaSalida());
     Date  fechaDate = dateFormat.parse(date);
    
     
     for(Libro li: listaLibros){
         if(prestamo.getLibro().getIdLibro()==li.getIdLibro()){
           prestamo.getLibro().setTitulo(li.getTitulo());
          }
     }
    for(Empleado e: listaEmpleados){
         if(prestamo.getEmpleado().getIdEmpleado()== e.getIdEmpleado()){
           prestamo.getEmpleado().setNombre(e.getNombre());
          }
     }
    
    for(Usuario u: listaUsuarios){
         if(prestamo.getUsuario().getIdUsuario()== u.getIdUsuario()){
           prestamo.getUsuario().setNombre(u.getNombre());
          }
     }
     prestamo.setFechaSalida(fechaDate);
   
      if(VerificaciOnExistenciaLibro(prestamo.getLibro())==true){
          //AUN HAY EN EXISTENCIA
         
          prestamoDao.agregar(prestamo);
          FacesMessage message = new FacesMessage("Se ha registro un nuevo Prestamo",usuario.getNombre() + " is uploaded.");
           FacesContext.getCurrentInstance().addMessage(null, message);  
            actualizarCantLibro(prestamo.getLibro());
           // llenarListas();
      }else{
      FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Libro Agotado", null);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, message);      
      }
    
    
    
    
    
         
        
}
   public void actualizarCantLibro(Libro libro){
    Libro libroAxu = new Libro();
    /*libroAxu.setIdLibro(libro.getIdLibro());
    libroAxu.setTitulo(libro.getTitulo());*/
 
     
   
    
     
    libroAxu = obtenerLibroaModificar(libro);
    libroAxu.setCantidd(libroAxu.getCantidd()-1); 
    libroDao.modificar(libroAxu);
   //  libroDao.modificarWeb(libroAxu.getIdLibro(),libroAxu.getCantidd());
    /*
       System.out.println(libroAxu.getIdLibro());
         System.out.println(libroAxu.getTitulo());
           System.out.println(libroAxu.getCategoria());
             System.out.println(libroAxu.getAnioEdicion());
               System.out.println(libroAxu.getCantidd());
                 System.out.println(Arrays.toString(libroAxu.getPortada()));*/
     //libroDao.modificar(libro);
     
     }
   
   public boolean VerificaciOnExistenciaLibro(Libro libro){
       boolean ban= false;
       
       for(Libro li: listaLibros){
         if(libro.getIdLibro()==li.getIdLibro()){
           if(li.getCantidd()>0){
               ban= true;
               System.out.println("Si hay existencia");
           }
          }
     }
       
       return ban;
   }
   public Libro obtenerLibroaModificar(Libro lib){
       Libro libro = new Libro();
         for(Libro li: listaLibros){
          if(lib.getIdLibro()==li.getIdLibro()){
            libro = li;  
          }; 
          
       }  
       return libro;
   }
   public void mostrarDatos(){
       /*for(Prestamo p: listaPrestamos){
           System.out.println(p.getEmpleado().getNombre()); 
            System.out.println(p.getUsuario().getNombre()); 
             System.out.println(p.getLibro().getTitulo()); 
       }
       */
       System.out.println(prestamo.getEmpleado().getNombre()); 
            System.out.println(prestamo.getUsuario().getNombre()); 
             System.out.println(prestamo.getLibro().getTitulo()); 
       
   }
   public void mostrarLibros(){
     for(Libro p: listaLibros){
           System.out.println(p.getPortada()); 
          
       }  
   }
   
   public void obtenerFEchaSalida(Date date){
       SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
       System.out.println(dateFormat.format(date));
       
       
       System.out.println(prestamo.getFechaSalida());
   }
   public void mostrar() throws ParseException{
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
       //System.out.println(dateFormat.format(prestamo.getFechaSalida()));
       String date =dateFormat.format(prestamo.getFechaSalida());
     Date  fechaDate = dateFormat.parse(date);
     
     prestamo.setFechaSalida(fechaDate);
      // System.out.println(prestamo.getFechaSalida());
}

   
   /*
public void mostrarJoin(){
    for(Prestamo p: listaPrestamos){
        System.out.println(p.getLibro().getTitulo());
    }
}*/
}

