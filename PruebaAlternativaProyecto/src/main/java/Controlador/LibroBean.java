/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javassist.bytecode.analysis.Analyzer;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import modelo.dao.AutorDao;
import modelo.entidad.Autor;
import modelo.entidad.Libro;

/**
 *
 * @author ADMIN
 */
@ManagedBean(name ="bkn_Lll")
@RequestScoped
public class LibroBean {

    /**
     * Creates a new instance of LaboratorioDesaUI
     */ 
   
    private List<Autor> lista;
    private List<Integer>ListaAnios;
  
    Libro libro;
    
    @PostConstruct
   public void init(){
     ListaAnios= new ArrayList();
     libro = new Libro();
     
     lista= new ArrayList();
      LlenarListaAnios();
     
   }

    public List<Integer> getListaAnios() {
        return ListaAnios;
    }

    public void setListaAnios(List<Integer> ListaAnios) {
        this.ListaAnios = ListaAnios;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public List<Autor> getLista() {
        return lista;
    }

    public void setLista(List<Autor> lista) {
        this.lista = lista;
    }

  
   public void mostrar(){
       for(Autor i: lista){
           System.out.println(i.getAutor());
       
       System.out.println(libro.getTitulo());
   }
   }
 
   public Integer obtenerFechaActual(){

        Calendar fecha = new GregorianCalendar();
                                                      
        int anio = fecha.get(Calendar.YEAR);
        System.out.println(anio);
        return anio;
       
   }
   public void LlenarListaAnios(){
       int anioInicio = 1960;
       for(Integer i: ListaAnios){
           if(anioInicio<=obtenerFechaActual()){
               System.out.println(i);
               ListaAnios.add(anioInicio);
             anioInicio = anioInicio + 1;   
           }
          
       }
   }
   public void detalle(){
       System.out.println(libro.getTitulo());
   }
   public void clear(){
   libro.setTitulo(null);
   libro.setPortada(null);
   
   }
   

}
   
  
