/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.utilidades.Persistencia;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;

import javax.faces.context.FacesContext;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;
import modelo.dao.AutorDao;
import modelo.dao.LibroDao;

import modelo.entidad.Autor;
import modelo.entidad.Libro;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import javax.servlet.ServletContext;
import modelo.dao.CategoriaDao;
import modelo.entidad.Categoria;
import org.apache.commons.io.FilenameUtils;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import modelo.dao.PuestoDao;
import modelo.entidad.Puesto;


/**
 *
 * @author elcon
 */
@ManagedBean(name ="puestoBean")
@RequestScoped
public class PuestoBean {
   private String previousPage = null;
 
   private List<Integer>listaAnios;
   private List<Libro>listaLibros;
   private List<Autor>listaAutores;
    private List<Categoria>listaCategorias;
   private Libro libro; 
   private LibroDao libroDao;
   private AutorDao autorDao;
   private CategoriaDao categoriaDao;
   private  ImageIcon icono;


   private UploadedFile file;  
   private StreamedContent imagem;
   
   private String imagenLibroTemporal;
   private String titul;

    private List<Puesto> listaPuestos;
    private PuestoDao puestoDao;
    private Puesto puesto;
    private Persistencia persistencia;
@PostConstruct
  public void init(){
  listaPuestos= new ArrayList();
  puesto = new Puesto();
  puestoDao = new PuestoDao();
  persistencia= new Persistencia();
  llenarListas();
   
  }

    public List<Puesto> getListaPuestos() {
        return listaPuestos;
    }

    public void setListaPuestos(List<Puesto> listaPuestos) {
        this.listaPuestos = listaPuestos;
    }

    public PuestoDao getPuestoDao() {
        return puestoDao;
    }

    public void setPuestoDao(PuestoDao puestoDao) {
        this.puestoDao = puestoDao;
    }

    public Puesto getPuesto() {
        return puesto;
    }

    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }
  
  
  
  
  public void llenarListas(){
       try {
           // PuestoDao puestodao = new PuestoDao();
           listaPuestos= persistencia.ConsumirListaPuestos();
       } catch (ParseException ex) {
           Logger.getLogger(PuestoBean.class.getName()).log(Level.SEVERE, null, ex);
       } catch (IOException ex) {
           Logger.getLogger(PuestoBean.class.getName()).log(Level.SEVERE, null, ex);
       } catch (org.json.simple.parser.ParseException ex) {
           Logger.getLogger(PuestoBean.class.getName()).log(Level.SEVERE, null, ex);
       }
   
}
public void puestoAgregar(){
    FacesMessage mensaje =new FacesMessage(); 
   // puestoDao = new PuestoDao();
    puestoDao.agregar(puesto);
    llenarListas();
    System.out.println(puesto.getPuesto());
    clear();
  
    FacesMessage message = new FacesMessage("Se ha registro un nuevo Puesto", puesto.getPuesto() + " is uploaded.");
    FacesContext.getCurrentInstance().addMessage(null, message);   
}
 public void clear(){
   puesto.setPuesto(null);
 
   }
public void men(){
    System.out.println("hola");
}

}

