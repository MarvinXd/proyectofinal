/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.utilidades.Persistencia;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import modelo.dao.CategoriaDao;

import modelo.entidad.Categoria;




/**
 *
 * @author elcon
 */
@ManagedBean(name ="bkn_categoria")
@RequestScoped
public class CategoriaBean {

    /**
     * Creates a new instance of LaboratorioDesaUI
     */
  
    private List<Categoria> listaCategorias;
    private Categoria categoria;
    private CategoriaDao categoriaDao;
    private Persistencia persistencia;

  
@PostConstruct
  public void init(){
  listaCategorias= new ArrayList();
  categoria= new Categoria();
  categoriaDao = new CategoriaDao();
  persistencia= new Persistencia();
  llenarListas();
    
  }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public List<Categoria> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(List<Categoria> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    public CategoriaDao getCategoriaDao() {
        return categoriaDao;
    }

    public void setCategoriaDao(CategoriaDao categoriaDao) {
        this.categoriaDao = categoriaDao;
    }


    
public void llenarListas(){
        try {
            listaCategorias = persistencia.ConsumirListaCategorias();
        } catch (ParseException ex) {
            Logger.getLogger(CategoriaBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CategoriaBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (org.json.simple.parser.ParseException ex) {
            Logger.getLogger(CategoriaBean.class.getName()).log(Level.SEVERE, null, ex);
        }
}
public void categoriaAgregar(){
   categoriaDao = new CategoriaDao();
   categoriaDao.agregar(categoria);
  
   
    FacesMessage mensaje =new FacesMessage(); 
   // puestoDao = new PuestoDao();
  
  
    FacesMessage message = new FacesMessage("Se ha registro un nueva categoria", categoria.getCategoria() + " is uploaded.");
    FacesContext.getCurrentInstance().addMessage(null, message); 
    clear();
}
 public void clear(){
  categoria.setCategoria(null);
   
   }
   
}
